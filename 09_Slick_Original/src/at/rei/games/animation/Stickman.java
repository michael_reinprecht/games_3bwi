package at.rei.games.animation;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.Animation;

public class Stickman extends BasicGame {

	private Animation animation;
	private float x, y;

	public void init(GameContainer c1) throws SlickException {
		SpriteSheet sheet = new SpriteSheet("C:\\Users\\Zechs\\Pictures\\Saved Pictures\\sword finished.png", 500, 375);
		animation = new Animation();
//		Image image234 = new Image("C:\\\\Users\\\\Zechs\\\\Pictures\\\\Backgrounds\\\\Auge.jpg");
		

		for (int i = 0; i < 10; i++) {
			animation.addFrame(sheet.getSprite(i, 0), 60);
		}
		animation.stopAt(9);

	}

	public Stickman() {
		super("Stickman");
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Stickman());
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	public void update(GameContainer gc, int delta) throws SlickException {
		
		if (gc.getInput().isKeyDown(Input.KEY_SPACE)) {
			animation.restart();
		}
		if (gc.getInput().isKeyDown(Input.KEY_RIGHT) == true) {
			this.x += delta * 0.1;
		}
		if (gc.getInput().isKeyDown(Input.KEY_LEFT) == true) {
			this.x -= delta * 0.1;
		}
		if (gc.getInput().isKeyDown(Input.KEY_UP) == true) {
			this.y -= delta * 0.1;
		}
		if (gc.getInput().isKeyDown(Input.KEY_DOWN) == true) {
			this.y += delta * 0.1;
		}
	}

	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		
		animation.draw(x, y);
//		graphics.drawImage(this.image234, (float)400, (float)400);
	}
}
