package at.rei.games.landscape;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class CreateRect implements Actor {
	private static final double SPEED = 0.005;
	private double y;
	private double x;
	private int height;
	private int width;
	private double angle;
	private Shape shape;
	

	public Shape getShape() {
		return shape;
	}


	public void setShape(Shape shape) {
		this.shape = shape;
	}


	public CreateRect(double y, double x, int height, int width) {
		super();
		this.y = y;
		this.x = x;
		this.height = height;
		this.width = width;
		this.shape = new Rectangle((float)this.x, (float)this.y, (float) this.width, (float)this.height);
	}

	
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.drawRect((int)this.x, (int)this.y, this.width, this.height);
		graphics.fillRect((int)this.x, (int)this.y, this.width, this.height);
		graphics.setColor(Color.lightGray);
	}



	
	private void moveRect(int delta) {
		// Rectmovement
		if (this.angle == 360) {
			this.angle = 0;
		}
		this.angle += delta * SPEED;

		this.x = 200 + 150 * Math.sin(angle);
		this.y = 200 + 150 * Math.cos(angle);
	}


	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		//moveRect(delta);
	}
	
}