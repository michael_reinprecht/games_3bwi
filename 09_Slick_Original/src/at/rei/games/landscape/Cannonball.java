package at.rei.games.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Cannonball implements Actor {
	private int width, height;
	private double x,y,speed;	
	
	public Cannonball(double x, double y, double speed) {
		super();
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.height = 20;
		this.width = 20;
	}
	
	
	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.fillOval((float)this.x, (float)this.y, width, height);
	}



	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		this.x += delta * 1;
	}

}
