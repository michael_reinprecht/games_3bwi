package at.rei.games.landscape;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AnimationTest;

public class Landscape extends BasicGame {

	// List of Actors
	private List<Actor> actors;
	private Player player;
	private List<Actor> newactors;

	public Landscape() {
		super("Landscape");
		// TODO Auto-generated constructor stub
	}

	public void init(GameContainer c1) throws SlickException {
		// Generates the ArrayList
		this.actors = new ArrayList<>();
		this.newactors = new ArrayList<>();
		this.player = new Player(200, 200, 30, 30, this);
		this.actors.add(this.player);
		CreateRect r1 = new CreateRect(200,200,200,200);
		this.actors.add(r1);
		player.addPartner(r1.getShape());
	}

	public void addnewActor(Actor actor) {
		this.newactors.add(actor);
	}
	
	

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Landscape());
			container.setDisplayMode(800, 600, false);
			container.start();
			container.setShowFPS(false);
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		// TODO Auto-generated method stub
		for (Actor actor : this.actors) {
			// Renders all Classes with the Interface Actor
			actor.render(graphics);
		}
	}

	public void update(GameContainer gc, int delta) throws SlickException {
		// TODO Auto-generated method stub
		for (Actor actor : this.actors) {
			// Updates all Classes with the Interface Actor
			actor.update(gc, delta);
		}

		if (this.newactors.size() > 0) {

			copyActorsFromNewToValid();
		}
	}

	private void copyActorsFromNewToValid() {
		for (Actor actor : this.newactors) {
			this.actors.add(actor);
		}
		this.newactors.clear();

	}
}
