package at.rei.games.landscape;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;

public class Player implements Actor {
	private double x, y;
	private double width, height;
	private boolean canGenerateBall = true;
	Landscape landscape;
	private Shape shape;
	private List<Shape> collisionPartner;

	public Player(double x, double y, double width, double height, Landscape landscape) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.landscape = landscape;
		this.shape = new Circle((float) this.x, (float) this.y, 10);
		this.collisionPartner = new ArrayList<>();
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	public void addPartner(Shape partner) {
		this.collisionPartner.add(partner);
	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.fillOval((int) this.x, (int) this.y, (int) this.width, (int) this.height);
		graphics.draw(this.shape);
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		
		
		for (Shape s1 : collisionPartner) {
			if (s1.intersects(this.shape)) {
				System.out.println("Collission!!!");
			}
		}
		
		if (gc.getInput().isKeyDown(Input.KEY_RIGHT) == true) {
			this.x += delta * 0.5;
		}
		if (gc.getInput().isKeyDown(Input.KEY_LEFT) == true) {
			this.x -= delta * 0.5;
		}
		
		if (gc.getInput().isKeyDown(Input.KEY_UP) == true) {
			this.y -= delta * 0.5;
		}
		if (gc.getInput().isKeyDown(Input.KEY_DOWN) == true) {
			this.y += delta * 0.5;
		}
		
		this.shape.setY((float)this.y);
		this.shape.setX((float)this.x);
		
		
		if (gc.getInput().isKeyDown(Input.KEY_SPACE) && canGenerateBall) {
			Cannonball cannonball = new Cannonball(this.x, this.y,5);
			this.landscape.addnewActor(cannonball);
			//this.canGenerateBall = false;
		}
	}

}
