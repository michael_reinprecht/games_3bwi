package at.rei.games.landscape;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import java.util.Random;

public class Snowflake implements Actor {

	//Chooses between big, medium and small snowflake
	private String size;
	//These types get Random generated at every single Snowflake
	private double x,y,speed,height,width;
	

	
	
	public Snowflake(String size) {
		super();
		this.size = size;
		//Random Generates Position of the Snowflakes
		this.x = Math.random()*800;
		this.y = Math.random()*-600;
		if (size == "big") {
			this.width = 20;
			this.height = 20;
			this.speed = 0.5;
		}
		if (size == "medium") {
			this.width = 10;
			this.height = 10;
			this.speed = 0.3;
		}
		if (size == "small") {
			this.width = 5;
			this.height = 5;
			this.speed = 0.15;
		}
	}

	

	

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.fillOval((float)this.x, (float)this.y, (float)this.width, (float)this.height);
	}
		
	
	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		this.y += delta * speed;
		if (this.y >= 600) {
			resetSnowflake();
		}		
	}


	//Reset the Snowflakes to a Random Position above the Game Screen
	private void resetSnowflake() {
		// TODO Auto-generated method stub
		this.x = Math.random()*800;
		this.y = Math.random()*-600;
	}
}
