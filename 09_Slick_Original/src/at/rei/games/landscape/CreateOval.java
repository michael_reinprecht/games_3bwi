package at.rei.games.landscape;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class CreateOval implements Actor {

	private static final double SPEED = 0.5;
	private int x, y, width, height;

	public CreateOval(int x, int y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.drawOval(this.x, this.y, this.width, this.height);
		graphics.fillOval(this.x, this.y, this.width, this.height);
		graphics.setColor(Color.white);
	}

	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		moveOval(delta);
	}

	private void moveOval(int delta) {
		// Ovalmovement
		if (this.y - 30 <= 600) {
			this.y += delta * SPEED;
		} else {
			this.y = 20;
		}
	}
}
