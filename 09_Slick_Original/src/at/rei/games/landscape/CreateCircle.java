package at.rei.games.landscape;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class CreateCircle implements Actor {
	private static final double SPEED = 0.3;
	private int x, y, width, height;
	private boolean goright = false;

	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.drawOval(this.x, this.y, this.width, this.height);
		graphics.fillOval(this.x, this.y, this.width, this.height);
		graphics.setColor(Color.cyan);
	}

	public CreateCircle(int x, int y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		moveCircle(delta);
	}
	

	private void moveCircle(int delta) {
		// Circlemovement
		if (this.x + 170 >= 800) {
			goright = false;
		}
		if (this.x <= 0) {
			goright = true;
		}

		if (goright == true) {
			this.x += delta * SPEED;
		}
		if (goright == false) {
			this.x -= delta * SPEED;
		}
	}
}
