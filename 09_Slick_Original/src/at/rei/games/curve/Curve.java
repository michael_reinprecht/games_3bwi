package at.rei.games.curve;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.Music;
import org.newdawn.slick.MusicListener;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Shape;
import at.rei.games.landscape.Actor;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Animation;

public class Curve extends BasicGame implements MusicListener {

	private List<Actor> actors;
	private CreatePlayer player1, player2;
	private List<Actor> newactors;
	private Image winimagered;
	private Image winimageblue;
	private boolean redwon, bluewon;
	private boolean start;
	private Animation deathblue, deathred;
	
	private String startmessage = "Press ENTER to start the Game \nRed Player Controls: A, D \nBlue Player Controls: LEFT, RIGHT";
	private String deathmessage = "";
	
	private Music music;
	private boolean musicEnded;
	private boolean musicSwapped;
	private boolean musicPlayed = false;
	
	public Curve() {
		super("Curve");
	}

	public void init(GameContainer c1) throws SlickException {
		// Create Actor and Newactorslist
		this.actors = new ArrayList<>();
		this.newactors = new ArrayList<>();
		
		music = new Music("testdata/restart.ogg", false);
		music.addListener(this);

		setInitGameObjects();
		setupGame();
	}

	private void setupGame() throws SlickException {
		this.actors.clear();
		this.newactors.clear();
		startmessage = "Press ENTER to start the Game \nRed Player Controls: A, D \nBlue Player Controls: LEFT, RIGHT";
		deathmessage = "";
		

		/* Create and add Players to Actors
		 * example:
		 * this.playernumber = new CreatePlayer(speed,this,Controlleft,Controlright,Playercolor);
		 *  */
		this.player1 = new CreatePlayer(0.2, this, Input.KEY_LEFT, Input.KEY_RIGHT, Color.blue);
		this.player2 = new CreatePlayer(0.2, this, Input.KEY_A, Input.KEY_D, Color.red);
		this.actors.add(player1);
		this.actors.add(player2);

		// Create and add a Border to Actors
		CreateBorder r1 = new CreateBorder();
		this.actors.add(r1);

		// Add Border Collision to Player
		player1.addBorder(r1.getShape());
		player2.addBorder(r1.getShape());

		musicPlayed = false;
		this.bluewon = false;
		this.redwon = false;
		this.start = false;
	}

	private void setInitGameObjects() throws SlickException {
		
		// Winimages
		winimageblue = new Image("C:\\Users\\Zechs\\git\\repository3\\09_Slick_Original\\testdata\\curvepictures\\bluewin.png");
		winimagered = new Image("C:\\Users\\Zechs\\git\\repository3\\09_Slick_Original\\testdata\\curvepictures\\redwin.png");

		// Deathanimations
		SpriteSheet sheetblue = new SpriteSheet("C:\\Users\\Zechs\\git\\repository3\\09_Slick_Original\\testdata\\curvepictures\\deathblue.png", 40, 40);
		SpriteSheet sheetred = new SpriteSheet("C:\\Users\\Zechs\\git\\repository3\\09_Slick_Original\\testdata\\curvepictures\\deathred.png", 40, 40);

		deathblue = new Animation();
		for (int i = 0; i < 20; i++) {
			deathblue.addFrame(sheetblue.getSprite(i, 0), 60);
		}
		deathblue.stopAt(19);

		deathred = new Animation();
		for (int i = 0; i < 20; i++) {
			deathred.addFrame(sheetred.getSprite(i, 0), 60);
		}
		deathred.stopAt(19);
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Curve());
			container.setDisplayMode(1920, 1080, true);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	public void update(GameContainer gc, int delta) throws SlickException {
		if (start == true) {

			if (bluewon == false && redwon == false) {
				for (Actor actor : this.actors) {
					// Renders all Classes with the Interface Actor
					actor.update(gc, delta);
				}
			}

			copyActorsFromNewToValid();
			getCollisions();
			isGameWon();
			exitGameIfESC(gc);
		}
	}

	public void render(GameContainer gc, Graphics graphics) throws SlickException {

		for (Actor actor : this.actors) {
			// Renders all Classes with the Interface Actor
			actor.render(graphics);
		}

		// Death/Startmessages
		graphics.drawString(startmessage, 800, 700);
		graphics.drawString(deathmessage, 820, 700);

		// Deathanimations/Pictures
		deathAnimations(graphics);

	}

	private void copyActorsFromNewToValid() {
		if (this.newactors.size() > 0) {

			for (Actor actor : this.newactors) {
				this.actors.add(actor);
			}
			this.newactors.clear();
		}

	}

	public void deathAnimations(Graphics graphics) {
		if (redwon == true) {
			deathblue.draw((float) player1.getX() - 14, (float) player1.getY() - 14);
			if (deathblue.getFrame() == 19) {
				player1.setX(2000);
				player1.setY(2000);
				player1.render(graphics);
			}
			if (musicPlayed == false)
			{
				music.play();
				musicPlayed = true;
			}
			winimagered.draw(0, 0);
			deathmessage = "Press ESC to exit the Game \nPress R to restart the Game";
			
		}
		if (bluewon == true) {
			deathred.draw((float) player2.getX() - 14, (float) player2.getY() - 14);
			if (deathred.getFrame() == 19) {
				player2.setX(2000);
				player2.setY(2000);
				player2.render(graphics);
			}
			if (musicPlayed == false)
			{
				music.play();
				musicPlayed = true;
			}
			winimageblue.draw(0, 0);
			deathmessage = "Press ESC to exit the Game \nPress R to restart the Game";
			
		}
	}

	public void getCollisions() {
		for (Shape s1 : player1.getCollisionTail()) {
			if ((s1.intersects(player2.getShape()))) {
				player1.setSpeed(0);
				player2.setSpeed(0);
				bluewon = true;
			}
			if ((s1.intersects(player1.getShape()))) {
				player1.setSpeed(0);
				player2.setSpeed(0);
				redwon = true;
			}
		}
		for (Shape s1 : player2.getCollisionTail()) {
			if ((s1.intersects(player1.getShape()))) {
				player1.setSpeed(0);
				player2.setSpeed(0);
				redwon = true;
			}
			if ((s1.intersects(player2.getShape()))) {
				player1.setSpeed(0);
				player2.setSpeed(0);
				bluewon = true;

			}
		}
	}

	public void exitGameIfESC(GameContainer gc) {
		if (gc.getInput().isKeyDown(Input.KEY_ESCAPE)) {
			System.exit(0);
		}
	}

	public void isGameWon() {
		if (player1.isWin() == true) {
			player1.setSpeed(0);
			player2.setSpeed(0);
			redwon = true;
		}
		if (player2.isWin() == true) {
			player1.setSpeed(0);
			player2.setSpeed(0);
			bluewon = true;
		}
	}

	public void mouseClicked(int button, int x, int y, int clickCount) {
		if (clickCount == 1) {
			startmessage = "";
			start = true;
		}
		if (clickCount == 2) {
			startmessage = "";
			start = true;
		}
	}

	public void addnewActor(Actor actor) {
		this.newactors.add(actor);
	}

	public boolean isRedwon() {
		return redwon;
	}

	public boolean isBluewon() {
		return bluewon;
	}

	
	
	@Override
	public void keyPressed(int key, char c) {
		// TODO Auto-generated method stub
		if (key == Input.KEY_R) {
			try {
				setupGame();
				musicEnded = false;
				musicSwapped = false;
				music.play();
			} catch (SlickException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (key == Input.KEY_ENTER) {
			musicEnded = false;
			musicSwapped = false;
			music.play();
			start = true;
			startmessage = "";
		}
	}

	@Override
	public void musicEnded(Music music) {
		// TODO Auto-generated method stub
		musicEnded = true;
	}

	@Override
	public void musicSwapped(Music music, Music newMusic) {
		// TODO Auto-generated method stub
		musicSwapped = true;
	}

}
