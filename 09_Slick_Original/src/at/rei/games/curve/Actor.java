package at.rei.games.curve;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public interface Actor {
	//Defines the Actors setup
	public void render(Graphics graphics);
	public void update(GameContainer gc, int delta);
}
