package at.rei.games.curve;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class Tail implements at.rei.games.landscape.Actor {

	private Color color;
	private double x, y;
	private Shape shape;

	public Tail(double x, double y, Color color) {
		super();
		this.x = x;
		this.y = y;
		this.color = color;
		this.shape = new Circle((float) this.x + 6, (float) this.y + 6, (float) 0.01);
	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.setColor(this.color);
		graphics.fillOval((float) this.x, (float) this.y, 5, 5);
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub

	}

	public Shape getShape() {
		return shape;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
}
