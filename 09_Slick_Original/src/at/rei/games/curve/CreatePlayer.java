package at.rei.games.curve;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;

public class CreatePlayer implements at.rei.games.landscape.Actor {

	private double angle = 2 * Math.PI * Math.random();;
	private double x = 200 + Math.random() * 800;
	private double y = 200 + Math.random() * 600;;
	private double speed;
	private Curve curve;
	private int leftkey;
	private int rightkey;
	private Color color;
	private Shape shape;
	private List<Shape> collisionBorder;
	private List<Shape> collisionTail;
	private boolean win = false;

	public CreatePlayer(double speed, Curve curve, int leftkey, int rightkey,
			Color color) {
		super();
		this.speed = speed;
		this.curve = curve;
		this.leftkey = leftkey;
		this.rightkey = rightkey;
		this.color = color;
		this.shape = new Circle((float) this.x, (float) this.y, 10);
		this.collisionBorder = new ArrayList<>();
		this.collisionTail = new ArrayList<>();
	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.setColor(this.color);
		graphics.fillOval((float) this.x, (float) this.y, 10, 10);
		Tail t1 = new Tail(this.x + 2.5 - Math.cos(angle) * 15, this.y + 2.5 - Math.sin(angle) * 15, this.color);
		this.curve.addnewActor(t1);
		this.addTail(t1.getShape());
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub

		for (Shape s1 : collisionBorder) {
			if (!(s1.intersects(this.shape)) && this.color == Color.blue) {
				System.out.println("ColissionBlue!!!");
				win = true;
			}
			if (!(s1.intersects(this.shape)) && this.color == Color.red) {
				System.out.println("ColissionRed!!!");
				win = true;
			}
		}
		if (curve.isBluewon() == false || curve.isRedwon() == false) {
			if (gc.getInput().isKeyDown(this.rightkey) == true) {
				this.angle += delta * 0.002;
			}
			if (gc.getInput().isKeyDown(this.leftkey) == true) {
				this.angle -= delta * 0.002;
			}
		}
		this.x += Math.cos(angle) * speed * delta;
		this.y += Math.sin(angle) * speed * delta;

		this.shape.setY((float) this.y);
		this.shape.setX((float) this.x);
	}

	public void addBorder(Shape partner) {
		this.collisionBorder.add(partner);
	}

	public void addTail(Shape partner) {
		this.collisionTail.add(partner);
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public List<Shape> getCollisionTail() {
		return collisionTail;
	}

	public Shape getShape() {
		return shape;
	}

	public boolean isWin() {
		return win;
	}
}
